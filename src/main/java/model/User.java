package model;

import java.sql.Date;
import java.util.List;

public class User {

    private Long id;
    private String firstName;
    private String lastName;
    private List<Contact> contactList;
    private Boolean isDeleted = false;
    private Date dateCreated;
    private Date dateDeleted;

    public User() {
    }

    public User(Long id, String firstName, String lastName, List<Contact> contactList, Boolean isDeleted, Date dateCreated, Date dateDeleted) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactList = contactList;
        this.isDeleted = isDeleted;
        this.dateCreated = dateCreated;
        this.dateDeleted = dateDeleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateDeleted() {
        return dateDeleted;
    }

    public void setDateDeleted(Date dateDeleted) {
        this.dateDeleted = dateDeleted;
    }
}
