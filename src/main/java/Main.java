import model.Contact;
import model.User;

import java.sql.Time;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Main {
    public static void main(String[] args) {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            public void run() {
                System.out.println("App Deleter beginning work");
                Logic logic = new Logic();
                //ищем и удаляем юзеров, которые подходят под условия(в статусе удаление больше чем 10 дней)
                List<User> list =  logic.getDeletedUsers();
                logic.deleteUsers(list);
                //ищем и удаляем контакты, которые подходят под условия(в статусе удаление больше чем 10 дней)
                List<Contact> contactList = logic.getDeletedContacts();
                logic.deleteContact(contactList);
                System.out.println("App Deleter is over!");
            }
        };

        Calendar date = Calendar.getInstance();
        int hour = date.get(Calendar.HOUR_OF_DAY);
        if (hour >= 18) {
            date.add(Calendar.DAY_OF_MONTH, 1);
            date.set(Calendar.HOUR_OF_DAY, 6);
        } else if (hour >= 6) {
            date.set(Calendar.HOUR_OF_DAY, 18);
        } else {
            date.set(Calendar.HOUR_OF_DAY, 6);
        }
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);

        timer.schedule(task, date.getTime(), 1000 * 60 * 60 * 12);
    }

}
