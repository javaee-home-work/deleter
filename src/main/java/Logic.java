import model.Contact;
import model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class Logic {


    public static Connection getConnection() {

        Connection connection = null;
        String url = "jdbc:postgresql://localhost:5432/contact";
        String user = "postgres";
        String password = "0191";

        try {
            connection = DriverManager.getConnection(url, user, password);
            if (connection != null) {
                System.out.println("Connected to the PostgreSQL server successfully.");
            } else {
                System.out.println("Failed to make connection!");
            }
        } catch (SQLException sqlException) {
            System.out.println(sqlException);
        }
        return connection;
    }

    public List<User> getDeletedUsers() {

        List<User> userList = new ArrayList<>();

        try {
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement("SELECT id,first_name,last_name,is_deleted,date_deleted FROM users WHERE is_deleted='true'");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getLong("id"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setDeleted(rs.getBoolean("is_deleted"));
                user.setDateDeleted(rs.getDate("date_deleted"));
                userList.add(user);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }

    public List<Contact> getDeletedContacts() {

        List<Contact> contactList = new ArrayList<>();

        try {
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement("SELECT id,first_name,last_name,is_deleted,date_deleted FROM contact WHERE is_deleted='true'");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Contact contact = new Contact();
                contact.setId(rs.getLong("id"));
                contact.setFirstName(rs.getString("first_name"));
                contact.setLastName(rs.getString("last_name"));
                contact.setDeleted(rs.getBoolean("is_deleted"));
                contact.setDateDeleted(rs.getDate("date_deleted"));
                contactList.add(contact);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return contactList;
    }

    public void deleteUsers(List<User> userList) {

        for (int i = 0; i < userList.size(); i++) {
            Date date1 = new Date(System.currentTimeMillis());
            Date date2 = Date.valueOf(String.valueOf(userList.get(i).getDateDeleted())); // какая-то другая дата
            long diffInMillies = Math.abs(date2.getTime() - date1.getTime());
            long diffInDays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            if (diffInDays >10) {
                int status = deleteUser(Collections.singletonList(userList.get(i).getId()),"user");
                if (status > 0) {
                    System.out.println("User was deleted user id: " + userList.get(i).getId());
                    System.out.println("User was deleted user firstName: " + userList.get(i).getFirstName());
                    System.out.println("User was deleted user lastName: " + userList.get(i).getLastName());
                } else {
                    System.out.println("Something Wrong");
                }
            }
        }

    }

    public void deleteContact(List<Contact> contactList) {

        for (int i = 0; i < contactList.size(); i++) {
            Date date1 = new Date(System.currentTimeMillis());
            Date date2 = Date.valueOf(String.valueOf(contactList.get(i).getDateDeleted())); // какая-то другая дата
            long diffInMillies = Math.abs(date2.getTime() - date1.getTime());
            long diffInDays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            if (diffInDays> 10) {
                int status = deleteUser(Collections.singletonList(contactList.get(i).getId()),"contact");
                if (status > 0) {
                    System.out.println("Contact was deleted contact id: " + contactList.get(i).getId());
                    System.out.println("Contact was deleted contact firstName: " + contactList.get(i).getFirstName());
                    System.out.println("Contact was deleted contact lastName: " + contactList.get(i).getLastName());
                } else {
                    System.out.println("Something Wrong");
                }
            }
        }

    }

    public int deleteUser(List<Long> ids, String value) {
        int status = 0;
        if (value.equals("user")) {
            try {
                Connection connection = getConnection();
                PreparedStatement ps1 = connection.prepareStatement("DELETE FROM email WHERE contact_id IN (SELECT id FROM contact WHERE user_id=?)");
                PreparedStatement ps2 = connection.prepareStatement("DELETE FROM numbers_of_phone WHERE contact_id IN (SELECT id FROM contact WHERE user_id=?)");
                PreparedStatement ps3 = connection.prepareStatement("DELETE FROM contact WHERE user_id=?");
                PreparedStatement ps4 = connection.prepareStatement("DELETE FROM users WHERE id=?");
                for (Long id : ids) {
                    ps1.setLong(1, id);
                    ps2.setLong(1, id);
                    ps3.setLong(1, id);
                    ps4.setLong(1, id);
                    status = ps1.executeUpdate();
                    System.out.println("Deleted email");
                    status = ps2.executeUpdate();
                    System.out.println("Deleted phone");
                    status = ps3.executeUpdate();
                    System.out.println("Deleted contact");
                    status = ps4.executeUpdate();
                    System.out.println("Deleted user");
                }
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return status;
        } else if (value.equals("contact")) {
            try {
                Connection connection = getConnection();
                PreparedStatement ps1 = connection.prepareStatement("DELETE FROM email WHERE contact_id=?");
                PreparedStatement ps2 = connection.prepareStatement("DELETE FROM numbers_of_phone WHERE contact_id=?");
                PreparedStatement ps3 = connection.prepareStatement("DELETE FROM contact WHERE id=?");
                for (Long id : ids) {
                    ps1.setLong(1, id);
                    ps2.setLong(1, id);
                    ps3.setLong(1, id);
                    status = ps1.executeUpdate();
                    System.out.println("Deleted email from contact where contact id: " + id);
                    status = ps2.executeUpdate();
                    System.out.println("Deleted phone number from contact where contact id: " + id);
                    status = ps3.executeUpdate();
                    System.out.println("Deleted contact from contact where contact id: " + id);
                }
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return status;
        }
        return status;
    }

}
